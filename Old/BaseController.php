<?php
class BaseController{

    private $viewPath;
    private $layoutPath;
    private $pageTitle = null;

    public function __construct(){
        $this->view = new \stdClass;
    }

    public function renderView($viewPath,$layoutPath = null){
        $this->viewPath = $viewPath;
        $this->layoutPath = $layoutPath;
        if($layoutPath){
            $this->layout();
        }else{
            $this->content();
        }
    }

    protected function content()
    {
        if(file_exists( __DIR__ . "/views/{$this->viewPath}.php")){
            return require_once (__DIR__ . "/views/{$this->viewPath}.php");
        } else{
            echo  "View Path not found.";
        }
    }

    protected function layout()
    {
        if(file_exists( __DIR__ . "/views/{$this->layoutPath}.php")){
            return require_once (__DIR__ . "/views/{$this->layoutPath}.php");
        } else{
            echo  "Layout Path not found.";
        }
    }
}

?>