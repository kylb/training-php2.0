<?php
include_once 'Mamifero.php';

    class Pessoa extends Mamifero
    {
        public $nome;
        public $especie;
        public $idade;
        private $algumacoisa;

        public static $respirar = "Verdade";

        const constante = "constante value";

        public function __construct($a = null, $b = null){
            $this->numeroOlhos = 2;
            if(!empty($a)){
                $this->nome = $a;
            }
            if(!empty($b)){
                $this->idade = $b;
            }
        }


        public static function especie(){
            echo "Humano";
        }

        public function comunicar(){
            parent::comunicar();
            echo "Digo palavras em português";
        }

        public function dizerOla(){
            echo "Olá, mundo. Meu nome é ". $this->nome;
        }

        public function setAlgumaCoisa($informacao){
            $this->algumacoisa = str_replace(';',' ', $informacao);
        }

        public function getAlgumaCoisa(){
            //return '"' . $this->algumacoisa . '"';
            return $this->colocarAspas($this->algumacoisa);
        }

        private function colocarAspas($item){
            return '"' . $item . '"';
        }

    }
?>