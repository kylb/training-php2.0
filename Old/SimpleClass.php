<?php
class SimpleClass
{
    // declaração de propriedade
    public $var;
    public $hora;

    function __construct(){
        //print "Eu crie esse objeto agora: " . date('d/m/Y h:m:s', time()) . "<br/>";
        $this->hora = date('d/m/Y  h:m:s',time());
    }
  
  // declaração de método
    public function displayVar() {
        echo $this->var;
    }

    public function displayDate() {
        echo "Esse objeto foi criado : " . $this->hora ;
    }
}
?>