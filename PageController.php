<?php
// include_once 'BaseController.php';
// extends BaseController
class PageController 
{
    // public function __construct(){
    //     parent::__construct();
    // }

    public function carregaPagina ($numPagina){
        switch ($numPagina) {
            case 1:
                require_once 'views/page1.php';
                break;
            case 2:
                require_once 'views/page2.php';
                break;
            default:
                require_once 'views/index.php';
                break;
        }
    }
}
?>